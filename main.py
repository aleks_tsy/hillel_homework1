def parse(query: str) -> dict:
    if "=" in query:
        splitted_by_question_query = query.split("?")
        splitted_by_question_query = splitted_by_question_query[1].split("&")
        result_dict = {}


        for par in splitted_by_question_query:
            splitted_par = par.split("=")
            if par == "":
                continue
            result_dict[splitted_par[0]] = splitted_par[1]

        return result_dict


    else:
        return {}


if __name__ == '__main__':
    assert parse('https://example.com/path/to/page?name=ferret&color=purple') == {'name': 'ferret', 'color': 'purple'}
    assert parse('https://example.com/path/to/page?name=ferret&color=purple&') == {'name': 'ferret', 'color': 'purple'}
    assert parse('http://example.com/') == {}
    assert parse('http://example.com/?') == {}
    assert parse('http://example.com/?name=Dima') == {'name': 'Dima'}
